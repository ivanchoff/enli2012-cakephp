enli2012-cakephp
================

Ejemplo de una aplicación CRUD completa utilizando CakePHP 2.2.3 para Enli2012.

La base de datos de ejemplo se encuentra en data/periodico.sql
El archivo cakephp-cakephp-2.2.3-0-g1234f96.zip corresponde al empaquetado de CakePHP2 estable al día de hoy.

Dudas:
Alex Arriaga
@alex_arriaga_m
http://fenix.cs.buap.mx